import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {KeyValueStoreService} from './services/key-value-store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'fe';
  addFormGroup: FormGroup = new FormGroup({
    keyToAdd: new FormControl(null),
    valueToAdd: new FormControl(null)
  });
  deleteFormGroup: FormGroup = new FormGroup({
    keyToDelete: new FormControl(null)
  });
  getFormGroup: FormGroup = new FormGroup({
    keyToGet: new FormControl(null)
  });

  isKeyDeleted: boolean;
  value: string;

  constructor(public service: KeyValueStoreService ) {
  }

  addKeyValue() {
    const keyToAdd = this.addFormGroup.get('keyToAdd').value;
    const valueToAdd = this.addFormGroup.get('valueToAdd').value;
    console.log('key:', keyToAdd);
    console.log('value:', valueToAdd);
    this.service.addKeyValue(keyToAdd, valueToAdd).subscribe();
  }

  deleteKeyValue() {
    const keyToDelete = this.deleteFormGroup.get('keyToDelete').value;
    this.service.deleteKeyValue(keyToDelete).subscribe();
  }

  getKeyValue() {
    const keyToGet = this.getFormGroup.get('keyToGet').value;
    console.log(keyToGet);
    this.service.getValue(keyToGet).subscribe((response) => this.value = response);
  }
}
