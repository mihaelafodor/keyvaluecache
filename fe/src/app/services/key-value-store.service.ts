import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class KeyValueStoreService {

  apiUrl = 'http://localhost:8080';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  // Create
  addKeyValue(key: string, value: string): Observable<any> {
    const API_URL = `${this.apiUrl}/addKeyValue`;
    const data = { key, value };

    return this.http.put(API_URL, data)
      .pipe(
        catchError(this.error)
      );
  }

  // Delete
  deleteKeyValue(key: string): Observable<any> {
    const API_URL = `${this.apiUrl}/deleteKeyValue/${key}`;

    return this.http.delete(API_URL)
      .pipe(
        catchError(this.error)
      );
  }

  // Get
  getValue(key: string): Observable<any> {
    const API_URL = `${this.apiUrl}/getValue/${key}`;

    return this.http.get(API_URL)
      .pipe(
        catchError(this.error)
      );
  }

  // Handle Errors
  error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
