import { TestBed } from '@angular/core/testing';

import { KeyValueStoreService } from './key-value-store.service';

describe('KeyValueStoreServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KeyValueStoreService = TestBed.get(KeyValueStoreService);
    expect(service).toBeTruthy();
  });
});
