package com.example.store.service;

import com.example.store.dto.KeyValue;
import org.springframework.stereotype.Service;

@Service
public class KeyValueStoreService {

    private LRUCache<String, String> cache;

    public KeyValueStoreService() {
        cache = new LRUCache<String, String>(4);
    }

    public KeyValue addKeyValue(KeyValue keyValue) {
        cache.put(keyValue.getKey(), keyValue.getValue());
        cache.printCache();
        return keyValue;
    }

    public String getValue(String key) {
        String value = cache.get(key);
        cache.printCache();
        return value;
    }

    public void deleteKeyValue(String key) {
        cache.delete(key);
        cache.printCache();
    }
}
