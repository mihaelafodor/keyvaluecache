package com.example.store.controller;

import com.example.store.dto.KeyValue;
import com.example.store.service.KeyValueStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class KeyValueStoreController {

    @Autowired
    KeyValueStoreService service;

    @PutMapping("/addKeyValue")
    public KeyValue addKeyValue(@RequestBody KeyValue keyValue) {
        return service.addKeyValue(keyValue);
    }

    @GetMapping("/getValue/{key}")
    public String getValue(@PathVariable String key) {
        return service.getValue(key);
    }

    @DeleteMapping("/deleteKeyValue/{key}")
    public void deleteKeyValue(@PathVariable String key) {
        service.deleteKeyValue(key);
    }
}